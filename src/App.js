import React, { Fragment, useState } from 'react';
import './App.css';
//routing
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router-dom';

//components
import AppNavbar from './components/AppNavbar';
//import Banner from './components/Banner';
//import Highlights from './components/Highlights';
//import Counter from './components/Counter';

//pages
import Home from './pages/Home';
import CoursePage from './pages/CoursePage';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import SpecificCourse from './pages/SpecificCourse';

//React Context
import UserContext from './UserContext';

//bootstrap
import { Container } from 'react-bootstrap';

//With the React Fragment component, we can group multiple components and avoid adding extra nodes to the DOM (documentation)

//<Fragment> is preferred to use

//short syntax - <> </> - shorter syntax you can use for declaring fragment. it doesn't support keys or attributes


function App() {
	//Browserouter/Router component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser
	//Switch component then declares with Route we can go to. Route component render the components within this container on the defined route
	//The exact param disables the partial matching for a route and makes sure that it only returns the route if the path is an EXACT match to the URL

	//We will use React Context to pass the state hook all through out our components. React context is nothing but a global state to the app. It is a way to make particular data available to all components no matter how they are nested. 

	//let's add a state hook for user, for now, let's set the default value to null
	const [user, setUser] = useState({
		//getItem() method returns value of the specified Storage Object item.
		email: localStorage.getItem('email'),
		accessToken: localStorage.getItem('accessToken'),
		isAdmin: localStorage.getItem('isAdmin') === 'true'
	});

	const unsetUser = () =>{
		localStorage.clear();
		setUser({ 
			email: null,
			accessToken: null,
			isAdmin: null
			 })
	};


/*
Activity

1. Create NotFound.js inside the page folder, when the user goes to a route that is not included in the Switch component, it should display Page Not Found/Error 404. Go back to the homepage(Link to "/")

2. Once done, save your commit message "Added NotFound for activity"

3. Copy and paste the link to boodle named "React js - Routing and Conditional Rendering"


*/
//<Route exact path="/register" component={user ? Home : Register}/>
//Provider component that allows consuming components to subscribe to context changes
  return (
  	<Fragment>
	  	<UserContext.Provider value={{ user, setUser, unsetUser }}>
	  		<Router>
		    	< AppNavbar />
		    	<Container>
			    	<Switch>
			    		< Route exact path="/login" component={Login}/>
			    		<Route exact path="/register">
			                {user.email !== null ? <Redirect to="/" /> : <Register />}
			            </Route>
				   		< Route exact path="/" component={Home} />
				   		< Route exact path="/courses" component={CoursePage} />
				   		< Route exact path="/courses/:courseId" component={SpecificCourse} />
				   		< Route component={NotFound} />
			    	</Switch>
		    	</Container>
	  		</Router>
	  	</UserContext.Provider>
  	</Fragment>
  );
}

export default App;
