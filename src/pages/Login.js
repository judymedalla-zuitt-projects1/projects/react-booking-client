import React, { useState, useEffect, useContext, Fragment } from 'react';
//bootstrap
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//React Context
import UserContext from '../UserContext';

import { Redirect, useHistory } from 'react-router-dom';


export default function Login(){

	//consume the UserContext object in the Login page via useContext()
	//useContext is a react hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.Provider component in App.js)
	const { user, setUser } = useContext(UserContext)

    const history = useHistory();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [loginButton, setLoginButton] = useState(false);

	useEffect(() => {
        if(email && password){
            setLoginButton(true)
        } else {
            setLoginButton(false)
        }
    }, [email, password, loginButton])


    function loginUser(e){
    	e.preventDefault();

        fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)

            //let's set our funcion in login page
            if(data.accessToken !== undefined){
                localStorage.setItem('accessToken', data.accessToken);
                setUser({ accessToken: data.accessToken })

                Swal.fire({
                    title: 'Yeeeeeeeeeey!',
                    icon: 'success',
                    text: 'Thank you for Logging in to Zuitt Booking System'
                })

                //fetch user's details from our token
                fetch('http://localhost:4000/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(result => {
                    console.log(result)

                    //we will check if the user is Admin or not, if admin, we will redirect to the /courses. if not, redirect to homepage.
                    if(result.isAdmin === true){

                        localStorage.setItem('email', result.email)
                        localStorage.setItem('isAdmin', result.isAdmin)
                        setUser({
                            email: result.email,
                            isAdmin: result.isAdmin
                        })
                        //we will redirect the page to /courses
                        history.push('/courses')

                    }else{
                        //redirect to homepage
                        history.push('/')
                    }

                })

            }else{
                Swal.fire({
                    title: 'Oooopps!',
                    icon: 'error',
                    text: 'Something Went Wrong. Check Your Credentials'
                })
            }
            setEmail('')
            setPassword('')
        })


    	/*Swal.fire({
    		title: "Yeeeeeeeeeey!",
    		icon: 'success',
    		text: 'Successfully Logged in'
    	})*/

    	//localStorage allows us to save data within our browsers as strings
    	//setItem() method of the storage interface, when passed a key name and value, it adds the key to the given Storage object or update that key's value if it already exists
    	//setItem() is used to store data in the localStorage as strings
    	//setItem('key', value)
    	/*localStorage.setItem('email', email);
    	setUser({email: email})

    	setEmail('')
        setPassword('')*/
    }

    if(user.email !== null) {
    	return <Redirect to="/" />
    }
/*

Fetch() is a method in JS, which allows to send a request to an api and process its response
syntax:


fetch(url, {options})
=url = resource/route from API
=optional object which contains additional information about our requests such the method, the body and the headers of our request


.then(res => res.json()) => parse the response as JSON
.then(result => { //process the response/result 
    console.log(result)
})


*/


	return(
		<Fragment>
			<h1 className="mt-5">Login</h1>
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control type="email" placeholder="Enter email here" value={email} onChange={e => setEmail(e.target.value)}/>
				</Form.Group>

				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter password here" value={password} onChange={e => setPassword(e.target.value)}/>
				</Form.Group>

				{loginButton ?
                    <Button variant="primary" type="submit">Submit</Button>
                    :
                    <Button variant="primary" type="submit" disabled>Submit</Button>
                }
			</Form>
		</Fragment>

		)
}