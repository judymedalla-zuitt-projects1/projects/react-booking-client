import React, { useState, useEffect, useContext} from 'react';
//bootstrap
import { Container } from 'react-bootstrap';
import UserContext from '../UserContext';

//components
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';



export default function CoursePage(){
	const { user } = useContext(UserContext);
	const [allCourses, setAllCourses] = useState([]);

	const fetchData = () =>{
		fetch(`${ process.env.REACT_APP_API_URL }/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllCourses(data)
		})
	}

	useEffect(()=>{
		fetchData()
	}, [])

	return(
		<Container>
			{
				(user.isAdmin === true) ?
				<AdminView courseData={ allCourses } fetchData={ fetchData }/>
				:
				<UserView courseData={ allCourses } />
			}
		</Container>

		)
}