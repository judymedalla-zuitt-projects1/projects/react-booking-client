import React from 'react';
import { Link } from 'react-router-dom';

import { Container } from 'react-bootstrap';

export default function NotFound(){
	return(
		
		<Container>
				<h3>Page Not Found</h3>
				<p>Go back to the <Link to="/">homepage</Link></p>
		</Container>

		)
}