import React, { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
//import Course from '../components/Course';
import Welcome from '../components/Welcome';

export default function Home(){
	return(
		<Fragment>
			< Banner />
			< Highlights />		
		</Fragment>

		)
}