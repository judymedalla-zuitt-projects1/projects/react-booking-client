import React, { useState, useEffect, useContext } from 'react';
//bootstrap
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

/*
Activity:

1. Using the code discussed earlier, update the Register Page component so that it sends the new user information to localhost:4000/users via POST endpoint


2. Add a fetch request to our register page.

	Create a new fetch request to the registerUser functionl
		-add the appropriate URL
		-add the appropriate options
	Then, process the result of the fetch using .json()
	Then, show the processed data in the console and a sweet alert that congratulates our user for registering.

3. Push your updates to your repo with commit message: "Fetch activity in Register js"
4. Copy and paste the link to boodle "React js API Integration with Fetch"


*/

export default function Register(){
	const { user } = useContext(UserContext)

	const history = useHistory();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');	
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');

	// The values in the fields of the form is bound to the getter of the state 
	//The event is bound to the setter. This is what we called two-way binding

	//The data we changed in the view has updated the state
	//Then data in the state has updated view
	const [registerButton, setRegisterButton] = useState(false)

	useEffect(()=>{
		if(email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword){
			setRegisterButton(true)
		}else{
			setRegisterButton(false)
		}

	}, [email, password, verifyPassword, registerButton])

	//We can declare multiple useEffect for each and every input state that we have so we can see the values change every key press

	useEffect(()=>{
		console.log(email);
	}, [email]);

	useEffect(()=>{
		console.log(password);
	}, [password]);

	useEffect(()=>{
		console.log(verifyPassword);
	}, [verifyPassword]);




	function registerUser(e){
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/users/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify ({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Successful Registration',
					icon: 'success',
					text: 'Thank you for registering'
				})

				history.push('/login')
			}else{
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
				})
			}
		})
	}

	/*if(user.email !== null){
		return < Redirect to="/" />
	}*/

/*
Activity:

1. Using the codes from the Register page component, create a Login.js in the pages folder and create a page that simulates the login using an email and password.
2. Update the App.js so that the Login page is shown.
3. Stretch goal: email and password validation, disable the button if the email and password is blank. 
4. use sweet alert
5. save your activity and write your commit message "Activity for React js effect events and forms" then push to your gitlab
6. Copy and paste it to boodle named "React js - Effects, Events and Forms"

*/


	return(
		<>
			<h1 className="mt-5">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required />
				</Form.Group>

				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last name" value={lastName} onChange={e => setLastName(e.target.value)} required />
				</Form.Group>

				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required />
					<Form.Text className="text-muted">
						We'll never share your email with any one else.
					</Form.Text>
				</Form.Group>

				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="text" placeholder="Enter 11-digit Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required />
				</Form.Group>

				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required />
				</Form.Group>
				<Form.Group>
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={e => setVerifyPassword(e.target.value)} required />
				</Form.Group>
				{registerButton ?   
					<Button variant="primary" type="submit">Submit</Button>
					 :
					<Button variant="primary" type="submit" disabled>Submit</Button>
				}


			</Form>
		</>


		)
}