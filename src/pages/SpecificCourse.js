import React, { useState, useEffect, useContext } from 'react';
//bootstrap
import { Container, Card, Button } from 'react-bootstrap';
//react-router
import { Link, useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function SpecificCourse(){
	const history = useHistory();

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	//useParams() contains any values we are trying to pass in the URL stored
	//useParams is how we receive the courseId passed via the URL
	const { courseId } = useParams();

	useEffect(()=>{

		fetch(`${ process.env.REACT_APP_API_URL }/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [])



	//for enroll button
	const enroll = (courseId) => {

		fetch('http://localhost:4000/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
					title: 'Successfully enrolled!',
					icon: 'success',
					text: 'You have successfully enrolled for this course.'
				})
				history.push('/courses')

			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})




	}


	return(
		<Container>
			<Card className="mt-5">
				<Card.Header className="bg-dark text-white text-center">
					<h4>{name}</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price} </h6>
				</Card.Body>

				<Card.Footer>

					{
						user.accessToken !== null ?
						<Button variant="primary" block onClick={() => enroll(courseId)}>
							Enroll
						</Button>
						:
						<Link className="btn btn-warning btn-block" to="/login">
							Login to Enroll
						</Link>
					}
					
				</Card.Footer>
			</Card>
		</Container>


		)
}
