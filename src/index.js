import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


/*Ctrl + shift + P -> Package Control: Install Package -> Babel*/

/*JSX Syntax or JavaScript XML is an extension to the syntax of JS. It allows us to write HTML-like syntax within our React.js projects and it includes JS features as well*/

//ReactDOM.render()
//The render() function from the imported ReactDOM module is the one responsible for injecting the whole React.js project to the web page.
ReactDOM.render(
    <App />
,
  document.getElementById('root')
);


