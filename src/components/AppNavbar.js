import React, { useContext } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';

//react-bootstrap
import Navbar from 'react-bootstrap/Navbar';//default export - change or placed it on any variable we want
import { Nav } from 'react-bootstrap';//named export - specific name/object that we used in a module

//React Context
import UserContext from '../UserContext';


export default function AppNavbar(){
	//The useHistory hook gives you access to the history instance that you may use to navigate/to access the location
	const history = useHistory();

	const { user, unsetUser } = useContext(UserContext);
	console.log(user)

	const logout = () =>{
		unsetUser();
		//push(path) - pushes a new entry onto the history stack
		history.push('/login')
	}

	//<NavLink to="/" activeClassName="selectedLink">Home</NavLink>
	//<Link to="/">Home</Link>

	//Apply conditional rendering in AppNavbar component such that a logout link will be shown instead of Login and Register when a user is logged in.
	let rightNav = (user.email !== null) ?
	(
		<>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
		</>
	)
	:
	(	
		<>
			<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
		</>
	)

	return(

		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/">
				React-Booking
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>	
				</Nav>
				<Nav className="ml-auto">
					{rightNav}
				</Nav>
			</Navbar.Collapse>
		</Navbar>



		)
}