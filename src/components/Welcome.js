import React from 'react';

export default function Welcome({name, age}){

	//props 
	//object for props: {props.name}
	return(
		<>
			<h1>Hello, {name}, Age: {age}</h1>
			<p>Welcome to our Course Booking</p>
		</>
		)
}