import React, { useState, useEffect } from 'react';
import { Col, Row, Card, Button } from 'react-bootstrap';
//Add PropTypes here to validate the props
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function Course({courseProp}) {

	//state = manage its own data
	//Hooks provide an alternative to write a class-based components
	//it helps us to write clearer and more concise code
	//Use a state hook here on Course component to demonstrate state by keeping track of number of enrollees.

	const { _id, name, description, price} = courseProp;

	//to change the state of the enrollees, we will use useState
	//The syntax of useState:
	//const [state, setState] = useState(initial State)
	//define 2 parameters[initial State(state getter), updated State(state setter -> change the value set by the initial state)]
	//useState(value)= the value inside this method is the initial or default value that the variable or component will be binded to
/*	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [disableBtn, setDisableBtn] = useState(false);*/
	//teh enrollee count will start at zero. The result of the useState() function is an array and the contents of the array is then destructured into two distinct identities, the count constant and setCount function.

	//we need to add an enrollee to the count when the Enroll button is clicked
	//function enroll(){
		//setCount(count + 1)//everytime the enroll button is clicked the number of enrollees per course increase by 1
	//}
/*	useEffect(()=>{
		if(seats === 0){
			alert(`No seats available`)
			setDisableBtn(true)
		}
	}, [setDisableBtn, seats])*/


	/*const enroll = () => {
            setCount(count + 1)
            setSeats(seats - 1)
    }*/

	/*
	Activity:

	1. Create a seats state in the Course component and set the initial value to 30.
	2. For every enrollment, deduct one to the seats.
	3. If the seats reaches zero:
		a. Do not add to the count.
		b. Do not deduct to the seats.
		c. Options for message:
			-Show an alert that says "no more seats".
			-the button wil be disabled (if the seats reaches zero)(hint: you will use state to set the button)
	4. save your activity and write your commit message "Activity for States" then push to your gitlab
	5. Copy and paste it to boodle named "React js - Props and State"

	*/
	return(

	<Row>
		<Col>
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>
						<h6>Description:</h6>
						<p>{description}</p>

						<h6>Price:</h6>
						<p>Php {price}</p>

						{/*<h6>{ count } Enrollees:</h6>
						<h6>{ seats } Seats Available:</h6>*/}
					</Card.Text>

					{/*<Button variant="primary" disabled={disableBtn} onClick={enroll}>
		              Enroll
		            </Button>*/}

		            <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>

				</Card.Body>
			</Card>
		</Col>
	</Row>


		)
}

	//We added an onClick event handler to the button that will trigger the enroll() function. The function then changes the value of the count state by adding + 1 to the value of the count.

	//In React Js, state values must not be changed directly. All changes to the state values must be through the included setState function when the state is created


	//Let us validate the data and its property types:
	//Purpose: To run a Type checking on the props for a component

	//propTypes property of our Course component will be set to an object with a key for each prop being passed to our component
	Course.propTypes = {
		//lets call on the property to check the data
		//shape() is used to check that a prop object conforms to a specific "shape"
		//course: => the props name
		course: PropTypes.shape({
			//define the properties and their expected types
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.number.isRequired
		})
	}