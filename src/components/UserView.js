import React, { useState, useEffect } from 'react';
import Course from './Course';



export default function UserView({courseData}){

	const [courses, setCourses] = useState([])

	useEffect(()=>{

		//map through the data we receive from the parent component, in order to render our courses page's card
		const coursesArr = courseData.map(course => {
			//only render active courses
			if(course.isActive === true){
				return(
					<Course key={course._id} courseProp={course}/>
					)
			}else{
				return null
			}
		});
		// set the courses state to the result of our map function, to bring our returned course components outside of the scope of our useEffect where our return statement below can see.
		setCourses(coursesArr)
	}, [courseData])

	return(
		<>
			{courses}
		</>

		)
}