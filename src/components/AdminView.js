import React, { useState, useEffect } from 'react';
//bootstrap
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){
	//Destructure our course data from the props being passed by the parent component

	const { courseData, fetchData } = props

	//add a state for courseId for the fetch URL
	const [courseId, setCourseId] = useState('')

	const [courses, setCourses] = useState([]);

	//Add state for Form in Add Course
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)

	//States for the Modal of Add Course
	const [showAdd, setShowAdd] = useState(false)
	//State for update Course modals(open/close)
	const [showEdit, setShowEdit] = useState(false)

	//Functions to handle opening and closing our Add Course Modal
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)


	//Function to handle opening our Edit Course Modal, which first needs to fetch the current course's data so that it can populate the value's of the inputs in the modal form
	const openEdit = (courseId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/courses/${ courseId }`)
		.then(res => res.json())
		.then(data => {
			//Populate all input values with the course information that we fetched
			setCourseId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		//Then, open the modal
		setShowEdit(true)
	}



	//Function to handle closing our Edit Course Modal. We need to reset all relevant states back to their default values, so that we can reuse them 
	const closeEdit = () =>{
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}




	useEffect(()=>{
		const coursesArr = courseData.map(course => {
			return(
				<tr key={course._id}>
					<td>{course._id}</td>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td className={course.isActive ? "text-success" : "text-danger"}>
						{course.isActive ? "Available" : "Unavailable"}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(course._id)}>
							Update
						</Button>
						{
							course.isActive
							?
							<Button variant="danger" size="sm" onClick={() => archiveToggle(course._id, course.isActive)}>
								Disable
							</Button>
							:
							<Button variant="success" size="sm" onClick={() => activateToggle(course._id, course.isActive)}>
								Enable
							</Button>

						}
					</td>
				</tr>


				)
		})

		setCourses(coursesArr)
	}, [courseData])



	//function for the Add course
	const addCourse = (e) => {
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/courses/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				//Run our fetchData function that we passed from our parent component, in order to re-render our page.
				fetchData()
				//Show a success message
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully added'
				})
				//Reset all states to their default values, for better user experience (inputs are blank again if the user decides to add another course)
				setName('')
				setDescription('')
				setPrice(0)

				//Close our modal
				closeAdd()
			}else{
				fetchData()
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})

	}


	//Update a Course
	const editCourse = (e, courseId) => {
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/courses/${ courseId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully updated'
				})
				closeEdit()
			}else {
				fetchData()
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

	//archive a course
	const archiveToggle = (courseId, isActive) => {
		fetch(`${ process.env.REACT_APP_API_URL }/${courseId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfuly unarchived'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

	const activateToggle = (courseId, isActive) => {
		fetch(`${ process.env.REACT_APP_API_URL }/courses/${courseId}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfuly unarchived'
				})
			}else{
				fetchData()
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}




	return(
		<>
			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>

				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={openAdd}>Add New Course</Button>
				</div>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{courses}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addCourse(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" placeholder="Enter Title of a Course" required value={name} onChange={e => setName(e.target.value)}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => setDescription(e.target.value)}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" required value={price} onChange={e => setPrice(e.target.value)}/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editCourse(e, courseId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Course</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" placeholder="Enter Title of a Course" required value={name} onChange={e => setName(e.target.value)}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => setDescription(e.target.value)}/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" required value={price} onChange={e => setPrice(e.target.value)}/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>





		</>

		)
}