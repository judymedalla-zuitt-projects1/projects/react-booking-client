//Effect Hooks, Events and Forms
//What does useEffect do? By using this Hook, you tell React that your component needs to do something after render. React will remember the function you passed(we'll refer to it as our 'side-effect') and call it later after performing the DOM updates
import React, { useState, useEffect } from 'react';

//bootstrap
import { Container, Button } from 'react-bootstrap';

export default function Counter(){
	const [count, setCount] = useState(0)
	//Using the useEffect requires two arguments: a function and an array of variables.
	//When the value of a variable in a given array is changed, the given function will be triggered

	useEffect(()=>{
		document.title = `You clicked ${count} times`
	}, [count])
	//In the case of the code above, whent the value of count has been changed, the page title will be updated with the number of times the button has been clicked.

	/*useEffect Dependency:

	useEffect allows us to perform tasks/functions on initial render:
		-When the component is displayed for the first time.

	What allows us to control when our useEffect will run AFTER the initial render?
		-we add an optional dependency array to control when useEffect will run, instead that it runs on initial render AND when states are updated, we can control the useEffect to run only when the state/s in the dependency array is updated.
	*/

	return(
		<Container>
			<p>You clicked {count} </p>
			<Button variant="primary" onClick={()=> setCount(count + 1)}>
				Click Me
			</Button>
		</Container>

		)
}